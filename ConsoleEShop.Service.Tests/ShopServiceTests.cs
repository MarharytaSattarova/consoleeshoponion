using ConsoleEShop.DataAccess.Entities;
using ConsoleEShop.DataAccess.Enums;
using ConsoleEShop.Service.Implementation;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace ConsoleEShop.Service.Tests
{
    public class ShopServiceTests
    {
        readonly ShopService _shopService;

        public ShopServiceTests()
        {
            _shopService = new ShopService();
        }

        [Fact]
        public void Shop_IsSingleton()
        {
            var newShopService = new ShopService();
            var product = new Product() { Name = "Name", Description = "Description", Category = ProductCategories.Category1, Price = 10.25M };
            _shopService.CreateProduct(product);

            Assert.Equal(newShopService.GetProducts().Count, _shopService.GetProducts().Count);
            Assert.Same(newShopService.GetProduct(product.Name), _shopService.GetProduct(product.Name));
        }

        [Theory]
        [InlineData("login", "password", "FName", "LName", "somemail@gmail.com")]
        [InlineData("01ue", "123456gh", "Name", "LastName", "_any@gmail.com")]
        public void Register_WhenValidUserInfo_ThenUsersCountChanged(string login, string password, string firstName, string lastName, string email)
        {
            var initialCount = _shopService.GetUsers().Count;
            var user = new User() { Login = login, Password = password, FirstName = firstName, LastName = lastName, Email = email };

            _shopService.Register(user);
            var resultCount = _shopService.GetUsers().Count;

            Assert.NotEqual(initialCount, resultCount);
        }

        [Theory]
        [InlineData("", "password", "FName", "LName", "somemail@gmail.com")]
        [InlineData("01ue", "", "Name", "", "any@gmail.com")]
        [InlineData("login", "pass", "Name", "LastName", "em")]
        public void Register_WhenInvalidUserInfo_ThenThrowArgumentException(string login, string password, string firstName, string lastName, string email)
        {
            var user = new User() { Login = login, Password = password, FirstName = firstName, LastName = lastName, Email = email };

            Assert.Throws<ArgumentException>(() => _shopService.Register(user));
        }

        [Theory]
        [InlineData("user1", "password")]
        [InlineData("User1Login", "123456")]
        [InlineData("admin", "111111")]
        public void LogIn_WhenNotExistingUser_ThenThrowInvalidOperationException(string login, string password)
        {
            Assert.Throws<InvalidOperationException>(() => _shopService.LogIn(login, password));
        }

        [Fact]
        public void GetProducts_WhenCalled_ThenReturnAListOfProducts()
        {
            var products = _shopService.GetProducts();

            Assert.IsType<List<Product>>(products);
        }

        [Theory]
        [InlineData("product1Name")]
        [InlineData("")]
        [InlineData("pn7")]
        public void GetProductByName_WhenNotExistingProduct_ThenThrowArgumentExceptionException(string name)
        {
            Assert.Throws<ArgumentException>(() => _shopService.GetProduct(name));
        }

        [Theory]
        [InlineData(14)]
        [InlineData(117)]
        public void GetProductById_WhenNotExistingProduct_ThenThrowArgumentException(int id)
        {
            Assert.Throws<ArgumentException>(() => _shopService.GetProduct(id));
        }

        [Theory]
        [InlineData(1, new int[] { 0, 1, 3 })]
        [InlineData(3, new int[] { 3, 2 })]
        public void CreateOrder_WhenValidOrderInfo_ThenGetProductsContainsAddedProduct(int userId, int[] productIds)
        {
            var order = new Order()
            {
                Items = new List<Product>() { new Product() { Name = "somename", Description = "somedescription", Price = 100.0M, Category = ProductCategories.Category1 } },
                Status = OrderStatus.New,
                UserId = userId
            };

            foreach (var item in productIds)
            {
                order.Items.Add(_shopService.GetProduct(item));
            }

            _shopService.CreateOrder(order);

            Assert.Contains(order, _shopService.GetUserOrders(userId));
        }

        [Theory]
        [InlineData(-1, new int[] { 7, 1, 3 })]
        [InlineData(8, new int[] { })]
        public void CreateOrder_WhenInvalidOrderInfo_ThenThrowInvalidOperationException(int userId, int[] productIds)
        {
            var order = new Order()
            {
                Items = new List<Product>(),
                Status = OrderStatus.New,
                UserId = userId
            };

            foreach (var item in productIds)
            {
                order.Items.Add(_shopService.GetProduct(item));
            }

            Assert.Throws<InvalidOperationException>(() => _shopService.CreateOrder(order));
        }

        [Theory]
        [InlineData(3, new int[] { 1, 0, 2 })]
        public void CancelOrder_WhenPossibleToCancel_ThenOrderStatusChanged(int userId, int[] productIds)
        {
            var order = new Order()
            {
                Items = new List<Product>(),
                Status = OrderStatus.New,
                UserId = userId
            };

            foreach (var item in productIds)
            {
                order.Items.Add(_shopService.GetProduct(item));
            }

            _shopService.CreateOrder(order);

            _shopService.CancelOrder(_shopService.GetUserInfo(userId), _shopService.GetUserOrders(userId).Find(x => x == order).Id);

            Assert.Equal(OrderStatus.CanceledByUser, order.Status);
        }

        [Theory]
        [InlineData(11, new int[] { 1, 0, 2 }, OrderStatus.Received)]
        [InlineData(26, new int[] { 6, 4 }, OrderStatus.CanceledByUser)]
        [InlineData(26, new int[] { 6, 4 }, OrderStatus.CanceledByAdmin)]
        public void CancelOrder_WhenNotPossibleToCancel_ThenThrowInvalidOperationException(int userId, int[] productIds, OrderStatus status)
        {
            var order = new Order()
            {
                Items = new List<Product>(),
                Status = OrderStatus.New,
                UserId = userId
            };

            foreach (var item in productIds)
            {
                order.Items.Add(_shopService.GetProduct(item));
            }

            var createdOrder = _shopService.CreateOrder(order);
            _shopService.GetUserOrders(userId).FirstOrDefault(x => x.Id == createdOrder.Id).Status = status;

            Assert.Throws<InvalidOperationException>(() => _shopService.CancelOrder(_shopService.GetUserInfo(userId), _shopService.GetUserOrders(userId).Find(x => x == order).Id));
        }

        [Theory]
        [InlineData(0, 1)]
        [InlineData(27, 0)]
        public void GetUserOrders_WhenExistingUserId_ThenReturnExpectedCount(int userId, int expectedOrdersCount)
        {
            var actualOrdersCount = _shopService.GetUserOrders(userId).Count;

            Assert.Equal(expectedOrdersCount, actualOrdersCount);
        }

        [Theory]
        [InlineData(115)]
        public void GetUserOrders_WhenNotExistingUserId_ThenThrowArgumentException(int userId)
        {
            Assert.Throws<ArgumentException>(() => _shopService.GetUserOrders(userId));
        }

        [Theory]
        [InlineData("SomeProductName", "SomeProductDescription", 34, ProductCategories.Category2)]
        [InlineData("NewProdName", "NewProdDesc", 201, ProductCategories.Category4)]
        public void CreateProduct_WhenValidProductInfo_ThenGetProductsContainsAddedProduct(string name, string description, decimal price, ProductCategories category)
        {
            var product = new Product() { Name = name, Description = description, Price = price, Category = category };

            _shopService.CreateProduct(product);

            Assert.Contains(product, _shopService.GetProducts());
        }

        [Theory]
        [InlineData("", "SomeProductDescription", 34, ProductCategories.Category2)]
        [InlineData("NewProdName", "NewProdDesc", 0.0, ProductCategories.Category1)]
        [InlineData("NewProdName", "NewProdDesc", -459, ProductCategories.Category3)]
        public void CreateProduct_WhenInvalidProductInfo_ThenThrowInvalidOperationException(string name, string description, decimal price, ProductCategories category)
        {
            var product = new Product() { Name = name, Description = description, Price = price, Category = category };

            Assert.Throws<InvalidOperationException>(() => _shopService.CreateProduct(product));
        }

        [Theory]
        [InlineData(3)]
        public void ProceedOrder_WhenValidOrderIdAndEnoughBalance_ThenUserBalanceHasCorrectValue(int userId)
        {
            var user = _shopService.GetUserInfo(userId);
            var order = new Order()
            {
                Id = 3,
                Items = new List<Product>() { new Product() { Name = "somename", Description = "somedescription", Price = 100.0M, Category = ProductCategories.Category1 } },
                Total = 500.09M,
                UserId = userId,
                Status = OrderStatus.New,
                CreatedAt = new DateTime(2020, 7, 26, 22, 22, 39)
            };
            _shopService.CreateOrder(order);
            var expectedBalance = user.Balance - order.Total;

            _shopService.ProceedOrder(user, order.Id);
            var actualBalance = user.Balance;

            Assert.Equal(expectedBalance, actualBalance);
        }

        [Theory]
        [InlineData(10)]
        public void ProceedOrder_WhenNotEnoughBalance_ThenThrowInvalidOperationException(int userId)
        {
            var user = _shopService.GetUserInfo(userId);
            user.Balance = 0.0M;
            var order = new Order()
            {
                Id = 3,
                Items = new List<Product>() { new Product() { Name = "somename", Description = "somedescription", Price = 100.0M, Category = ProductCategories.Category1 } },
                Total = 500.09M,
                UserId = userId,
                Status = OrderStatus.New,
                CreatedAt = new DateTime(2020, 7, 26, 22, 22, 39)
            };

            _shopService.CreateOrder(order);

            Assert.Throws<InvalidOperationException>(() => _shopService.ProceedOrder(user, order.Id));
        }

        [Theory]
        [InlineData(-10, 3)]
        public void ProceedOrder_WhenInvalidOrderId_ThenThrowArgumentException(int orderId, int userId)
        {
            var user = _shopService.GetUserInfo(userId);

            Assert.Throws<ArgumentException>(() => _shopService.ProceedOrder(user, orderId));
        }

        [Theory]
        [InlineData(17, OrderStatus.Sent)]
        [InlineData(21, OrderStatus.PaymentReceived)]
        public void SetOrderStatus_WhenValidOrderId_ThenOrderStatusChanged(int userId, OrderStatus newStatus)
        {
            var order = new Order()
            {
                Items = new List<Product>() { new Product() { Name = "somename", Description = "somedescription", Price = 100.0M, Category = ProductCategories.Category1 } },
                UserId = userId,
            };

            var newOrderInfo = _shopService.CreateOrder(order);
            var initialOrderStatus = newOrderInfo.Status;

            _shopService.SetOrderStatus(newOrderInfo.Id, newStatus);

            Assert.NotEqual(initialOrderStatus, newOrderInfo.Status);
            Assert.Equal(newStatus, newOrderInfo.Status);
        }

        [Theory]
        [InlineData(-1, OrderStatus.Sent)]
        [InlineData(211, OrderStatus.PaymentReceived)]
        public void SetOrderStatus_WhenInvalidOrderId_ThenThrowArgumentException(int orderId, OrderStatus newStatus)
        {
            Assert.Throws<ArgumentException>(() => _shopService.SetOrderStatus(orderId, newStatus));
        }

        [Fact]
        public void GetUsers_WhenCalled_ThenReturnAListOfUsers()
        {
            var users = _shopService.GetUsers();

            Assert.IsType<List<User>>(users);
        }

        [Theory]
        [InlineData(25, "Alfred", "Ward", "Alfred_Ward44@yahoo.com")]
        [InlineData(11, "Sherman", "Kuhlman", "Sherman_Kuhlman44@hotmail.com")]
        public void GetUserInfo_WhenExistingUserId_ThenReturnExpectedResult(int userId, string firstName, string lastName, string email)
        {
            var user = _shopService.GetUserInfo(userId);

            Assert.Equal(firstName, user.FirstName);
            Assert.Equal(lastName, user.LastName);
            Assert.Equal(email, user.Email);
        }

        [Theory]
        [InlineData(-25)]
        [InlineData(150)]
        public void GetUserInfo_WhenNotExistingUserId_ThenThrowArgumentException(int userId)
        {
            Assert.Throws<ArgumentException>(() => _shopService.GetUserInfo(userId));
        }

        [Theory]
        [InlineData(4, "NewFName", "NewLName", "Dianna_Lynch@hotmail.com", "newLogin", "_1212 134")]
        [InlineData(17, "fName", "lName", "Beatrice.Kerluke@gmail.com", "98713Login", "_1231jbui")]
        public void UpdateUserInfo_WhenIdAndBodyAreValid_ThenUserInfoChanged(int userId, string newFirstName, string newLastName, string newEmail, string newLogin, string newPass)
        {
            var newUserInfo = new User() { FirstName = newFirstName, LastName = newLastName, Email = newEmail, Login = newLogin, Password = newPass };
            var userInfoBeforeUpdate = _shopService.GetUserInfo(userId);

            var newUserInfoList = new List<string>()
            {
                newUserInfo.FirstName,
                newUserInfo.LastName,
                newUserInfo.Email,
                newUserInfo.Login,
                newUserInfo.Password
            };

            var userInfoBeforeUpdateList = new List<string>()
            {
                userInfoBeforeUpdate.FirstName,
                userInfoBeforeUpdate.LastName,
                userInfoBeforeUpdate.Email,
                userInfoBeforeUpdate.Login,
                userInfoBeforeUpdate.Password
            };

            _shopService.UpdateUserInfo(userId, newUserInfo);
            var userInfoAfterUpdate = _shopService.GetUserInfo(userId);

            var userInfoAfterUpdateList = new List<string>()
            {
                userInfoAfterUpdate.FirstName,
                userInfoAfterUpdate.LastName,
                userInfoAfterUpdate.Email,
                userInfoAfterUpdate.Login,
                userInfoAfterUpdate.Password
            };

            Assert.NotEqual(userInfoBeforeUpdateList, userInfoAfterUpdateList);
            Assert.Equal(newUserInfoList, userInfoAfterUpdateList);
        }

        [Theory]
        [InlineData(1, "", "NewLName", "Dianna_Lynch@hotmail.com", "newLogin", "_1212 134")]
        [InlineData(23, "fName", " ", "Beatrice.Kerluke@gmail.com", "98713Login", "_1231jbui")]
        [InlineData(13, "fName", "lName", "str", "98713Login", "_1231jbui")]
        [InlineData(11, "NewFName", "NewLName", "Dianna_Lynch@hotmail.com", " ", "_1212 134")]
        [InlineData(8, "newName", "NewLName", "Dianna_Lynch@hotmail.com", "newLogin", "")]
        public void UpdateUserInfo_WhenBodyIsInvalid_ThenThrowInvalidOperationException(int userId, string newFirstName, string newLastName, string newEmail, string newLogin, string newPass)
        {
            var newUserInfo = new User() { FirstName = newFirstName, LastName = newLastName, Email = newEmail, Login = newLogin, Password = newPass };

            Assert.Throws<InvalidOperationException>(() => _shopService.UpdateUserInfo(userId, newUserInfo));
        }

        [Theory]
        [InlineData(-1)]
        [InlineData(125)]
        public void UpdateUserInfo_WhenIdIsInvalid_ThenThrowArgumentExcetion(int userId)
        {
            var newUserInfo = new User() { FirstName = "newFirstName", LastName = "newLastName", Email = "newEmail@gmail.com", Login = "newLogin", Password = "newPass" };

            Assert.Throws<ArgumentException>(() => _shopService.UpdateUserInfo(userId, newUserInfo));
        }

        [Theory]
        [InlineData("Product9Name", "NewProdName", "NewProdDescription", 23.50, ProductCategories.Category3)]
        [InlineData("Product2Name", "Prod1Name", "101", 0.50, ProductCategories.Category1)]
        public void UpdateProductInfo_WhenBodyIsValid_ThenProductInfoChanged(string productName, string newProductName, string newProductDescription, decimal newProdusctPrice, ProductCategories newProductCategory)
        {
            var newProductInfo = new Product() { Name = newProductName, Description = newProductDescription, Price = newProdusctPrice, Category = newProductCategory };
            var productInfoBeforeUpdate = _shopService.GetProduct(productName);

            var newProductInfoList = new List<string>()
            {
                newProductInfo.Name,
                newProductInfo.Description,
                newProductInfo.Price.ToString(),
                newProductInfo.Category.ToString()
            };

            var productInfoBeforeUpdateList = new List<string>()
            {
                productInfoBeforeUpdate.Name,
                productInfoBeforeUpdate.Description,
                productInfoBeforeUpdate.Price.ToString(),
                productInfoBeforeUpdate.Category.ToString()
            };

            _shopService.UpdateProductInfo(productInfoBeforeUpdate.Id, newProductInfo);
            var productInfoAfterUpdate = _shopService.GetProduct(productInfoBeforeUpdate.Id);

            var productInfoAfterUpdateList = new List<string>()
            {
                productInfoAfterUpdate.Name,
                productInfoAfterUpdate.Description,
                productInfoAfterUpdate.Price.ToString(),
                productInfoAfterUpdate.Category.ToString()
            };

            Assert.NotEqual(productInfoBeforeUpdateList, productInfoAfterUpdateList);
            Assert.Equal(newProductInfoList, productInfoAfterUpdateList);
        }

        [Theory]
        [InlineData("Product1Name", "", "NewProdDescription", 23.50, ProductCategories.Category3)]
        [InlineData("Product10Name", "Prod10Name", "", 0.50, ProductCategories.Category1)]
        [InlineData("Product7Name", "7Name", "NewProdDescription", -100.0, ProductCategories.Category4)]
        [InlineData("Product4Name", "ProdName", "NewProdDescription", 0.0, ProductCategories.Category2)]
        public void UpdateProductInfo_WhenBodyIsInvalid_ThenThrowInvalidOperationException(string productName, string newProductName, string newProductDescription, decimal newProdusctPrice, ProductCategories newProductCategory)
        {
            var newProductInfo = new Product() { Name = newProductName, Description = newProductDescription, Price = newProdusctPrice, Category = newProductCategory };
            var searchedProduct = _shopService.GetProduct(productName);

            Assert.Throws<InvalidOperationException>(() => _shopService.UpdateProductInfo(searchedProduct.Id, newProductInfo));
        }

        [Theory]
        [InlineData(14, 25.0, 103.0)]
        [InlineData(25, 100.5, 110.5)]
        public void TopUpUserBalance_WhenValidAmount_ThenUserBalanceChanged(int userId, decimal amountToPutOnBalance, decimal expectedAmount)
        {
            var searchedUser = _shopService.GetUserInfo(userId);

            _shopService.TopUpUserBalance(amountToPutOnBalance, searchedUser);
            var resultingUserBalance = searchedUser.Balance;

            Assert.Equal(expectedAmount, resultingUserBalance);
        }

        [Theory]
        [InlineData(1, -120.0)]
        [InlineData(29, 0)]
        public void TopUpUserBalance_WhenInvalidAmount_ThenThrowInvalidOperationException(int userId, decimal amountToPutOnBalance)
        {
            var searchedUser = _shopService.GetUserInfo(userId);

            Assert.Throws<InvalidOperationException>(() => _shopService.TopUpUserBalance(amountToPutOnBalance, searchedUser));
        }
    }
}
