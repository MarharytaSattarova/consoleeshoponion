﻿namespace ConsoleEShop.DataAccess.Enums
{
    /// <summary>
    /// Represents various order status options (New, CanceledByAdmin, CanceledByUser, PaymentReceived, Sent, Received and Completed).
    /// </summary>
    public enum  OrderStatus
    {
        /// <summary>
        /// Set when order has just been created.
        /// </summary>
        New,
        /// <summary>
        /// Set when admin canceles the order.
        /// </summary>
        CanceledByAdmin,
        /// <summary>
        /// Set when ordinary user canceles the order.
        /// </summary>
        CanceledByUser,
        /// <summary>
        /// Set by admin after user proceeds the order.
        /// </summary>
        PaymentReceived,
        /// <summary>
        /// Set by admin when order is passed to shipping.
        /// </summary>
        Sent,
        /// <summary>
        /// Set by user after the order gets shipped to them.
        /// </summary>
        Received,
        /// <summary>
        /// Set by admin after user's confirmation that he/she received the order.
        /// </summary>
        Completed
    }
}
