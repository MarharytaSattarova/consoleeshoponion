﻿namespace ConsoleEShop.DataAccess.Enums
{
    /// <summary>
    /// Represents various roles that users can have in the system (Guest, RegisteredUser and Admin).
    /// </summary>
    public enum UserRoles
    {
        /// <summary>
        /// Is given to the user that does not have account in the system.
        /// </summary>
        Guest,
        /// <summary>
        /// Is given to the user when he/she creates an account in the system.
        /// </summary>
        RegisteredUser,
        /// <summary>
        /// Is given to the person who have administrator rights in the system.
        /// </summary>
        Admin
    }
}
