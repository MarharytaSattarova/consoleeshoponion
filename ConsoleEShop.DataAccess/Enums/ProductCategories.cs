﻿namespace ConsoleEShop.DataAccess.Enums
{
    /// <summary>
    /// Represents various product categories options (Category1, Category2, Category3, Category4).
    /// </summary>
    public enum ProductCategories
    {
        /// <summary>
        /// Product category option.
        /// </summary>
        Category1,
        /// <summary>
        /// Product category option.
        /// </summary>
        Category2,
        /// <summary>
        /// Product category option.
        /// </summary>
        Category3,
        /// <summary>
        /// Product category option.
        /// </summary>
        Category4
    }
}
