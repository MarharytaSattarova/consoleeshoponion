﻿using ConsoleEShop.DataAccess.Enums;

namespace ConsoleEShop.DataAccess.Entities
{
    /// <summary>
    /// Business entity containing all the information about the product.
    /// </summary>
    public class Product : BaseEntity
    {
        /// <summary>
        /// Stores product name.
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Stores additional information about the product.
        /// </summary>
        public string Description { get; set; }
        /// <summary>
        /// Stores product price.
        /// </summary>
        public decimal Price { get; set; }
        /// <summary>
        /// Stores the category to which product belongs to.
        /// </summary>
        public ProductCategories Category { get; set; }
    }
}
