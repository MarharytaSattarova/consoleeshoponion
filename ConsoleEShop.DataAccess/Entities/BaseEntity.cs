﻿namespace ConsoleEShop.DataAccess.Entities
{
    /// <summary>
    /// Serves as the base class to all the business entities present in the program.
    /// </summary>
    public class BaseEntity
    {
        /// <summary>
        /// Stores the unique Id for each instance.
        /// </summary>
        public int Id { get; set; }
    }
}
