﻿using ConsoleEShop.DataAccess.Enums;
using System;

namespace ConsoleEShop.DataAccess.Entities
{
    /// <summary>
    /// Business entity containing all the information about the user.
    /// </summary>
    public class User : BaseEntity
    {
        /// <summary>
        /// Stores user's login.
        /// </summary>
        public string Login { get; set; }
        /// <summary>
        /// Stores user's password.
        /// </summary>
        public string Password { get; set; }
        /// <summary>
        /// Stores user's first name.
        /// </summary>
        public string FirstName { get; set; }
        /// <summary>
        /// Stores user's last name.
        /// </summary>
        public string LastName { get; set; }
        /// <summary>
        /// Stores user's email.
        /// </summary>
        public string Email { get; set; }
        /// <summary>
        /// Stores user's role in the system (Guest, Registered User or Admin).
        /// </summary>
        public UserRoles Role { get; set; }
        /// <summary>
        /// Stores the date and time when user registered in the system.
        /// </summary>
        public DateTime RegisteredAt { get; set; }
        /// <summary>
        /// Stores user's current amount of money on the balance.
        /// </summary>
        public decimal Balance { get; set; }
    }
}
