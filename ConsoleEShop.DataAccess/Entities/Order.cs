﻿using ConsoleEShop.DataAccess.Enums;
using System;
using System.Collections.Generic;

namespace ConsoleEShop.DataAccess.Entities
{
    /// <summary>
    /// Business entity containing all the information about the order.
    /// </summary>
    public class Order : BaseEntity
    {
        /// <summary>
        /// Stores the list of products included in order.
        /// </summary>
        public List<Product> Items { get; set; }
        /// <summary>
        /// Stores order total calculated by adding prices of the products in the corresponding list.
        /// </summary>
        public decimal Total { get; set; }
        /// <summary>
        /// Stores order current status (New, CanceledByAdmin, CanceledByUser, PaymentReceived, Sent, Received or Completed).
        /// </summary>
        public OrderStatus Status { get; set; }
        /// <summary>
        /// Stores the date and time when order was created.
        /// </summary>
        public DateTime CreatedAt { get; set; }
        /// <summary>
        /// Stores the date and time when order was finished (applied automatically when order status is set to Complited).
        /// </summary>
        public DateTime? FinishedAt { get; set; }
        /// <summary>
        /// Stores Id of the user who created the order.
        /// </summary>
        public int UserId { get; set; }
    }
}
