﻿using ConsoleEShop.DataAccess.Entities;
using ConsoleEShop.Repository.Context;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ConsoleEShop.Repository.Repositories
{
    /// <summary>
    /// Repository to access and modify user instances in the application context.
    /// </summary>
    public class UserRepository : BaseRepository<User>
    {
        /// <summary>
        /// Construstor that creates an instance of the user repository based on the ApplicationContext instance passed as argument.
        /// </summary>
        /// <param name="context">Application context instance with seed information.</param>
        public UserRepository(ApplicationContext context) : base(context) { }
        /// <summary>
        /// Searches for the user by Id and deletes user instance from the collection of users if it's found.
        /// </summary>
        /// <param name="id">Id of the user that has to be deleted.</param>
        /// <exception cref="ArgumentException">Thrown when user with the provided Id is not found in the shop."</exception>
        public override void Delete(int id)
        {
            var itemToDelete = context.Users.FirstOrDefault(x => x.Id == id);

            if (itemToDelete == null) throw new InvalidOperationException("User with such Id does not exist.");

            context.Users.Remove(itemToDelete);
        }
        /// <summary>
        /// Gets all the users present in the shop.
        /// </summary>
        /// <returns>List of users.</returns>
        public override List<User> Get()
        {
            return context.Users;
        }
        /// <summary>
        /// Gets the user with specified Id.
        /// </summary>
        /// <param name="id">Id of the user to find and return.</param>
        /// <returns>User instance or null if the user with given Id does not exist in the collection.</returns>
        public override User Get(int id)
        {
            var searchedUser = context.Users.FirstOrDefault(x => x.Id == id);

            if (searchedUser == null) throw new ArgumentException("User with such Id not found.", "id");

            return searchedUser;
        }
        /// <summary>
        /// Searches for an user by Id and updates it's information with the information provided in the user object argument.
        /// </summary>
        /// <param name="entity">User instance containig new user information.</param>
        /// <param name="id">Id of the user that has to be updated.</param>
        /// <exception cref="ArgumentException">Thrown when user with the provided Id is not found in the shop."</exception>
        public override void Update(User entity, int id)
        {
            var searchedUser = context.Users.FirstOrDefault(x => x.Id == id);

            if (searchedUser == null) throw new ArgumentException("User with such Id not found.", "id");

            searchedUser.Login = entity.Login;
            searchedUser.Password = entity.Password;
            searchedUser.FirstName = entity.FirstName;
            searchedUser.LastName = entity.LastName;
            searchedUser.Email = entity.Email;
        }
    }
}
