﻿using ConsoleEShop.DataAccess.Entities;
using ConsoleEShop.Repository.Context;
using ConsoleEShop.Repository.Interfaces;
using System.Collections.Generic;

namespace ConsoleEShop.Repository.Repositories
{
    /// <summary>
    /// Serves as the base class for all the repositories.
    /// </summary>
    /// <typeparam name="TEntity">Generic parameter which specifies the entity type.</typeparam>
    public abstract class BaseRepository<TEntity> : IRepository<TEntity> where TEntity : BaseEntity
    {
        /// <summary>
        /// Stores the instance of the application context containing collections of all users, products and orders present in the system.
        /// </summary>
        protected readonly ApplicationContext context;
        /// <summary>
        /// Constructor that creates instance of the base repository based on the context passed as an argument.
        /// </summary>
        /// <param name="context">Application context instance with seed information.</param>
        protected BaseRepository(ApplicationContext context)
        {
            this.context = context;
        }
        /// <summary>
        /// Adds provided entity to the collection of entities of the same type.
        /// </summary>
        /// <param name="entity">Entity instance to add to the collection.</param>
        public virtual void Create(TEntity entity)
        {
            switch (entity)
            {
                case User user:
                    context.Users.Add(user);
                    break;
                case Order order:
                    context.Orders.Add(order);
                    break;
                case Product product:
                    context.Products.Add(product);
                    break;
            }
        }
        /// <summary>
        /// Deletes the given entity instance from the collection of entities of the same type.
        /// </summary>
        /// <param name="entity">Entity instance to delete from the collection.</param>
        public virtual void Delete(TEntity entity)
        {
            switch (entity)
            {
                case User user:
                    context.Users.Remove(user);
                    break;
                case Order order:
                    context.Orders.Remove(order);
                    break;
                case Product product:
                    context.Products.Remove(product);
                    break;
            }
        }
        /// <summary>
        /// Searches for the entity by Id and deletes entity instance from the collection of entities of the same type if it's found.
        /// </summary>
        /// <param name="id">Id of the entity that has to be deleted.</param>
        public abstract void Delete(int id);
        /// <summary>
        /// Gets all the entities of the specified type.
        /// </summary>
        /// <returns>List of entities.</returns>
        public abstract List<TEntity> Get();
        /// <summary>
        /// Gets the entity with specified Id.
        /// </summary>
        /// <param name="id">Id of the entity to find and return.</param>
        /// <returns>Entity or null if the entity with given Id does not exist in the collection.</returns>
        public abstract TEntity Get(int id);
        /// <summary>
        /// Searches for an entity by Id and updates it's information with the information provided in the entity object argument.
        /// </summary>
        /// <param name="entity">Entity instance containig new entity information.</param>
        /// <param name="id">Id of the entity that has to be updated.</param>
        public abstract void Update(TEntity entity, int id);
    }
}
