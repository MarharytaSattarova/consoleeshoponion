﻿using ConsoleEShop.DataAccess.Entities;
using ConsoleEShop.Repository.Context;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ConsoleEShop.Repository.Repositories
{
    /// <summary>
    /// Repository to access and modify order instances in the application context.
    /// </summary>
    public class OrderRepository : BaseRepository<Order>
    {
        /// <summary>
        /// Construstor that creates an instance of the order repository based on the ApplicationContext instance passed as argument.
        /// </summary>
        /// <param name="context">Application context instance with seed information.</param>
        public OrderRepository(ApplicationContext context) : base(context) { }
        /// <summary>
        /// Searches for the order by Id and deletes order instance from the collection of orders if it's found.
        /// </summary>
        /// <param name="id">Id of the order that has to be deleted.</param>
        /// <exception cref="ArgumentException">Thrown when order with the provided Id is not found in the shop."</exception>
        public override void Delete(int id)
        {
            var itemToDelete = context.Orders.FirstOrDefault(x => x.Id == id);

            if (itemToDelete == null) throw new ArgumentException("Order with such Id does not exist.", "id");

            context.Orders.Remove(itemToDelete);
        }
        /// <summary>
        /// Gets all the orders present in the shop.
        /// </summary>
        /// <returns>List of orders.</returns>
        public override List<Order> Get()
        {
            return context.Orders;
        }
        /// <summary>
        /// Gets the order with specified Id.
        /// </summary>
        /// <param name="id">Id of the order to find and return.</param>
        /// <returns>Order instance or null if the order with given Id does not exist in the collection.</returns>
        public override Order Get(int id)
        {
            return context.Orders.FirstOrDefault(x => x.Id == id);
        }
        /// <summary>
        /// Searches for an order by Id and updates it's information with the information provided in the order object argument.
        /// </summary>
        /// <param name="entity">Order instance containig new order information.</param>
        /// <param name="id">Id of the order that has to be updated.</param>
        /// <exception cref="ArgumentException">Thrown when order with the provided Id is not found in the shop."</exception>
        public override void Update(Order entity, int id)
        {
            var searchedOrder = context.Orders.FirstOrDefault(x => x.Id == id);

            if (searchedOrder == null) throw new ArgumentException("Order with such Id not found.", "id");

            searchedOrder.Items = entity.Items;
            searchedOrder.Status = entity.Status;
            searchedOrder.Total = entity.Total;
            searchedOrder.UserId = entity.UserId;
        }
    }
}
