﻿using ConsoleEShop.DataAccess.Entities;
using ConsoleEShop.Repository.Context;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ConsoleEShop.Repository.Repositories
{
    /// <summary>
    /// Repository to access and modify product instances in the application context.
    /// </summary>
    public class ProductRepository : BaseRepository<Product>
    {
        /// <summary>
        /// Construstor that creates an instance of the product repository based on the ApplicationContext instance passed as argument.
        /// </summary>
        /// <param name="context">Application context instance with seed information.</param>
        public ProductRepository(ApplicationContext context) : base(context) { }
        /// <summary>
        /// Searches for the product by Id and deletes product instance from the collection of products if it's found.
        /// </summary>
        /// <param name="id">Id of the product that has to be deleted.</param>
        /// <exception cref="ArgumentException">Thrown when product with the provided Id is not found in the shop."</exception>
        public override void Delete(int id)
        {
            var itemToDelete = context.Products.FirstOrDefault(x => x.Id == id);

            if (itemToDelete == null) throw new ArgumentException("Product with such Id does not exist.", "id");

            context.Products.Remove(itemToDelete);
        }
        /// <summary>
        /// Gets all the products present in the shop.
        /// </summary>
        /// <returns>List of products.</returns>
        public override List<Product> Get()
        {
            return context.Products;
        }
        /// <summary>
        /// Gets the product with specified Id.
        /// </summary>
        /// <param name="id">Id of the product to find and return.</param>
        /// <returns>Product instance or null if the product with given Id does not exist in the collection.</returns>
        public override Product Get(int id)
        {
            return context.Products.FirstOrDefault(x => x.Id == id);
        }
        /// <summary>
        /// Searches for an product by Id and updates it's information with the information provided in the product object argument.
        /// </summary>
        /// <param name="entity">product instance containig new product information.</param>
        /// <param name="id">Id of the product that has to be updated.</param>
        /// <exception cref="ArgumentException">Thrown when product with the provided Id is not found in the shop."</exception>
        public override void Update(Product entity, int id)
        {
            var searchedProduct = context.Products.FirstOrDefault(x => x.Id == id);

            if (searchedProduct == null) throw new ArgumentException("Product with such Id not found.", "id");

            searchedProduct.Name = entity.Name;
            searchedProduct.Description = entity.Description;
            searchedProduct.Price = entity.Price;
            searchedProduct.Category = entity.Category;
        }
    }
}
