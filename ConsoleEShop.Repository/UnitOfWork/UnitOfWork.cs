﻿using ConsoleEShop.DataAccess.Entities;
using ConsoleEShop.Repository.Context;
using ConsoleEShop.Repository.Interfaces;
using ConsoleEShop.Repository.Repositories;

namespace ConsoleEShop.Repository.UnitOfWork
{
    /// <summary>
    /// Provides access to the data of all the repositories in the application.
    /// </summary>
    public class UnitOfWork : IUnitOfWork
    {
        /// <summary>
        /// Stores an instance of the application context.
        /// </summary>
        private readonly ApplicationContext _context;
        /// <summary>
        /// Stores the instance of order repository.
        /// </summary>
        private OrderRepository _orderRepository;
        /// <summary>
        /// Stores the instance of product repository.
        /// </summary>
        private ProductRepository _productRepository;
        /// <summary>
        /// Stores the instance of user repository.
        /// </summary>
        private UserRepository _userRepository;
        /// <summary>
        /// Constructor that creates an instance of UnitOfWork.
        /// </summary>
        public UnitOfWork()
        {
            _context = new ApplicationContext();
        }
        /// <summary>
        /// Stores an instance that implements <see cref="IRepository{User}"/> interface and provides access only for reading. If the instance of user repository does not exist, than creates it.
        /// </summary>
        public IRepository<User> Users
        {
            get
            {
                if (_userRepository == null)
                    _userRepository = new UserRepository(_context);
                return _userRepository;
            }
        }
        /// <summary>
        /// Stores an instance that implements <see cref="IRepository{Order}"/> interface and provides access only for reading. If the instance of order repository does not exist, than creates it.
        /// </summary>
        public IRepository<Order> Orders
        {
            get
            {
                if (_orderRepository == null)
                    _orderRepository = new OrderRepository(_context);
                return _orderRepository;
            }
        }
        /// <summary>
        /// Stores an instance that implements <see cref="IRepository{Product}"/> interface and provides access only for reading. If the instance of product repository does not exist, than creates it.
        /// </summary>
        public IRepository<Product> Products
        {
            get
            {
                if (_productRepository == null)
                    _productRepository = new ProductRepository(_context);
                return _productRepository;
            }
        }
    }
}
