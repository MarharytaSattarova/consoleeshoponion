﻿using ConsoleEShop.DataAccess.Entities;

namespace ConsoleEShop.Repository.Interfaces
{
    /// <summary>
    /// Contains the functionality that UnitOfWork must have.
    /// </summary>
    public interface IUnitOfWork
    {
        /// <summary>
        /// Stores an instance that implements <see cref="IRepository{User}"/> interface and provides access only for reading.
        /// </summary>
        IRepository<User> Users { get; }
        /// <summary>
        /// Stores an instance that implements <see cref="IRepository{Order}"/> interface and provides access only for reading.
        /// </summary>
        IRepository<Order> Orders { get; }
        /// <summary>
        /// Stores an instance that implements <see cref="IRepository{Product}"/> interface and provides access only for reading.
        /// </summary>
        IRepository<Product> Products { get; }
    }
}
