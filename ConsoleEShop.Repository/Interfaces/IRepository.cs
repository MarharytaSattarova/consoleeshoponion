﻿using System.Collections.Generic;

namespace ConsoleEShop.Repository.Interfaces
{
    /// <summary>
    /// Contains the functionality that Repository must have.
    /// </summary>
    /// <typeparam name="TEntity">Generic parameter which specifies the entity type.</typeparam>
    public interface IRepository<TEntity>
    {
        /// <summary>
        /// Gets all the entities of the specified type.
        /// </summary>
        /// <returns>List of entities.</returns>
        List<TEntity> Get();
        /// <summary>
        /// Gets the entity with specified Id.
        /// </summary>
        /// <param name="id">Id of the entity to find and return.</param>
        /// <returns>Entity or null if the entity with given Id does not exist in the collection.</returns>
        TEntity Get(int id);
        /// <summary>
        /// Adds provided entity to the collection of entities of the same type.
        /// </summary>
        /// <param name="entity">Entity instance to add to the collection.</param>
        void Create(TEntity entity);
        /// <summary>
        /// Searches for an entity by Id and updates it's information with the information provided in the entity object argument.
        /// </summary>
        /// <param name="entity">Entity instance containig new entity information.</param>
        /// <param name="id">Id of the entity that has to be updated.</param>
        void Update(TEntity entity, int id);
        /// <summary>
        /// Deletes the given entity instance from the collection of entities of the same type.
        /// </summary>
        /// <param name="entity">Entity instance to delete from the collection.</param>
        void Delete(TEntity entity);
        /// <summary>
        /// Searches for the entity by Id and deletes entity instance from the collection of entities of the same type if it's found.
        /// </summary>
        /// <param name="id">Id of the entity that has to be deleted.</param>
        void Delete(int id);
    }
}
