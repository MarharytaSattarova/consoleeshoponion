﻿using ConsoleEShop.DataAccess.Entities;
using ConsoleEShop.DataAccess.Enums;
using System;
using System.Collections.Generic;

namespace ConsoleEShop.Repository.Context
{
    /// <summary>
    /// Stores seeding data for the shop.
    /// </summary>
    public static class DataSource
    {
        /// <summary>
        /// Seeding data containing user profiles information.
        /// </summary>
        public static List<User> Users { get; set; } = new List<User>()
        {
            new User()
            {
                Id = 0,
                FirstName = "Jordane",
                LastName = "Walker",
                Email = "Jordane.Walker@gmail.com",
                Login = "User0Login",
                Password = "User0Password",
                RegisteredAt = new DateTime(2020, 6, 24, 12, 11, 10, 569, DateTimeKind.Local).AddTicks(1173),
                Role = UserRoles.RegisteredUser,
                Balance = 250.80M
            },

            new User()
            {
                Id = 1,
                FirstName = "Kolby",
                LastName = "Jones",
                Email = "Kolby_Jones@yahoo.com",
                Login = "User1Login",
                Password = "User1Password",
                RegisteredAt = new DateTime(2020, 6, 27, 23, 20, 55, 794, DateTimeKind.Local).AddTicks(233),
                Role = UserRoles.Admin,
                Balance = 0.0M
            },

            new User()
            {
                Id = 2,
                FirstName = "Mabelle",
                LastName = "Miller",
                Email = "Mabelle.Miller12@hotmail.com",
                Login = "User2Login",
                Password = "User2Password",
                RegisteredAt = new DateTime(2020, 6, 22, 15, 35, 42, 583, DateTimeKind.Local).AddTicks(4106),
                Role = UserRoles.RegisteredUser,
                Balance = 50.30M
            },

            new User()
            {
                Id = 3,
                FirstName = "Verla",
                LastName = "Bechtelar",
                Email = "Verla.Bechtelar62@gmail.com",
                Login = "User3Login",
                Password = "User3Password",
                RegisteredAt = new DateTime(2020, 7, 1, 8, 22, 15, 869, DateTimeKind.Local).AddTicks(5560),
                Role = UserRoles.RegisteredUser,
                Balance = 1117.90M
            },

            new User()
            {
                Id = 4,
                Email = "Retha67@yahoo.com",
                FirstName = "Retha",
                LastName = "Will",
                Login = "User4Login",
                Password = "User4Password",
                RegisteredAt = new DateTime(2020, 6, 23, 21, 9, 18, 503, DateTimeKind.Local).AddTicks(8054),
                Role = UserRoles.RegisteredUser,
                Balance = 1121.00M
            },

            new User()
            {
                Id = 5,
                Email = "Roberto.Ruecker35@yahoo.com",
                FirstName = "Roberto",
                LastName = "Ruecker",
                RegisteredAt = new DateTime(2020, 6, 2, 2, 17, 26, 430, DateTimeKind.Local).AddTicks(9832),
                Login = "User5Login",
                Password = "User5Password",
                Role = UserRoles.RegisteredUser,
                Balance = 131.00M
            },

            new User()
            {
                Id = 6,
                Email = "Tracy.Schinner65@gmail.com",
                FirstName = "Tracy",
                LastName = "Schinner",
                RegisteredAt = new DateTime(2020, 6, 23, 17, 37, 38, 49, DateTimeKind.Local).AddTicks(8331),
                Login = "User6Login",
                Password = "User6Password",
                Role = UserRoles.RegisteredUser,
                Balance = 631.00M
            },

            new User()
            {
                Id = 7,
                Email = "Norma_Gislason@yahoo.com",
                FirstName = "Norma",
                LastName = "Gislason",
                RegisteredAt = new DateTime(2020, 5, 28, 20, 2, 2, 295, DateTimeKind.Local).AddTicks(5080),
                Login = "User7Login",
                Password = "User7Password",
                Role = UserRoles.RegisteredUser,
                Balance = 651.00M
            },

            new User()
            {
                Id = 8,
                Email = "Luisa92@yahoo.com",
                FirstName = "Luisa",
                LastName = "Lind",
                RegisteredAt = new DateTime(2020, 6, 22, 22, 42, 43, 764, DateTimeKind.Local).AddTicks(2460),
                Login = "User8Login",
                Password = "User8Password",
                Role = UserRoles.RegisteredUser,
                Balance = 2231.00M
            },

            new User()
            {
                Id = 9,
                Email = "Edward_Kuvalis@yahoo.com",
                FirstName = "Edward",
                LastName = "Kuvalis",
                RegisteredAt = new DateTime(2020, 6, 3, 2, 16, 31, 267, DateTimeKind.Local).AddTicks(7270),
                Login = "User9Login",
                Password = "User9Password",
                Role = UserRoles.RegisteredUser,
                Balance = 253.00M
            },

            new User()
            {
                Id = 10,
                Email = "Jayde22@hotmail.com",
                FirstName = "Jayde",
                LastName = "Pouros",
                RegisteredAt = new DateTime(2020, 6, 29, 16, 40, 13, 993, DateTimeKind.Local).AddTicks(9520),
                Login = "User10Login",
                Password = "User10Password",
                Role = UserRoles.RegisteredUser,
                Balance = 2351.00M
            },

            new User()
            {
                Id = 11,
                Email = "Sherman_Kuhlman44@hotmail.com",
                FirstName = "Sherman",
                LastName = "Kuhlman",
                RegisteredAt = new DateTime(2020, 6, 12, 1, 12, 48, 711, DateTimeKind.Local).AddTicks(8931),
                Login = "User11Login",
                Password = "User11Password",
                Role = UserRoles.RegisteredUser,
                Balance = 754.00M
            },

            new User()
            {
                Id = 12,
                Email = "Issac.Rath@gmail.com",
                FirstName = "Issac",
                LastName = "Rath",
                RegisteredAt = new DateTime(2020, 6, 18, 10, 48, 28, 954, DateTimeKind.Local).AddTicks(2609),
                Login = "User12Login",
                Password = "User12Password",
                Role = UserRoles.RegisteredUser,
                Balance = 245.00M
            },

            new User()
            {
                Id = 13,
                Email = "Kirsten_Braun49@gmail.com",
                FirstName = "Kirsten",
                LastName = "Braun",
                RegisteredAt = new DateTime(2020, 6, 3, 17, 20, 9, 673, DateTimeKind.Local).AddTicks(5222),
                Login = "User13Login",
                Password = "User13Password",
                Role = UserRoles.RegisteredUser,
                Balance = 754.00M
            },

            new User()
            {
                Id = 14,
                Email = "Tamara58@gmail.com",
                FirstName = "Tamara",
                LastName = "Brakus",
                RegisteredAt = new DateTime(2020, 6, 8, 16, 4, 31, 614, DateTimeKind.Local).AddTicks(4169),
                Login = "User14Login",
                Password = "User14Password",
                Role = UserRoles.RegisteredUser,
                Balance = 78.00M
            },

            new User()
            {
                Id = 15,
                Email = "Hilton.Kemmer84@gmail.com",
                FirstName = "Hilton",
                LastName = "Kemmer",
                RegisteredAt = new DateTime(2020, 7, 1, 10, 0, 16, 686, DateTimeKind.Local).AddTicks(452),
                Login = "User15Login",
                Password = "User15Password",
                Role = UserRoles.RegisteredUser,
                Balance = 4.00M
            },

            new User()
            {
                Id = 16,
                Email = "Delmer9@gmail.com",
                FirstName = "Delmer",
                LastName = "Wunsch",
                RegisteredAt = new DateTime(2020, 5, 28, 0, 57, 23, 539, DateTimeKind.Local).AddTicks(7637),
                Login = "User16Login",
                Password = "User16Password",
                Role = UserRoles.RegisteredUser,
                Balance = 982.00M
            },

            new User()
            {
                Id = 17,
                Email = "Lupe91@gmail.com",
                FirstName = "Lupe",
                LastName = "Harber",
                RegisteredAt = new DateTime(2020, 5, 22, 20, 21, 56, 99, DateTimeKind.Local).AddTicks(8576),
                Login = "User17Login",
                Password = "User17Password",
                Role = UserRoles.RegisteredUser,
                Balance = 91.00M
            },

            new User()
            {
                Id = 18,
                Email = "Cedrick33@hotmail.com",
                FirstName = "Cedrick",
                LastName = "Lemke",
                RegisteredAt = new DateTime(2020, 5, 22, 7, 53, 58, 617, DateTimeKind.Local).AddTicks(4950),
                Login = "User18Login",
                Password = "User18Password",
                Role = UserRoles.RegisteredUser,
                Balance = 982.00M
            },

            new User()
            {
                Id = 19,
                Email = "Zane.Will@yahoo.com",
                FirstName = "Zane",
                LastName = "Will",
                RegisteredAt = new DateTime(2020, 6, 5, 14, 13, 47, 441, DateTimeKind.Local).AddTicks(5614),
                Login = "User19Login",
                Password = "User19Password",
                Role = UserRoles.RegisteredUser,
                Balance = 91.00M
            },

            new User()
            {
                Id = 20,
                Email = "Shane67@gmail.com",
                FirstName = "Shane",
                LastName = "Windler",
                RegisteredAt = new DateTime(2020, 6, 13, 23, 22, 28, 486, DateTimeKind.Local).AddTicks(5957),
                Login = "User20Login",
                Password = "User20Password",
                Role = UserRoles.RegisteredUser,
                Balance = 282.00M
            },

            new User()
            {
                Id = 21,
                Email = "Verdie.Gutmann46@hotmail.com",
                FirstName = "Verdie",
                LastName = "Gutmann",
                RegisteredAt = new DateTime(2020, 6, 21, 18, 57, 15, 537, DateTimeKind.Local).AddTicks(2226),
                Login = "User21Login",
                Password = "User21Password",
                Role = UserRoles.RegisteredUser,
                Balance = 9123.00M
            },

            new User()
            {
                Id = 22,
                Email = "Abe_Dooley66@gmail.com",
                FirstName = "Abe",
                LastName = "Dooley",
                RegisteredAt = new DateTime(2020, 6, 22, 1, 43, 12, 738, DateTimeKind.Local).AddTicks(6212),
                Login = "User22Login",
                Password = "User22Password",
                Role = UserRoles.RegisteredUser,
                Balance = 0.0M
            },

            new User()
            {
                Id = 23,
                Email = "Bud.Collins@gmail.com",
                FirstName = "Bud",
                LastName = "Collins",
                RegisteredAt = new DateTime(2020, 6, 9, 21, 35, 51, 844, DateTimeKind.Local).AddTicks(5877),
                Login = "User23Login",
                Password = "User23Password",
                Role = UserRoles.RegisteredUser,
                Balance = 23.00M
            },

            new User()
            {
                Id = 24,
                Email = "Columbus.Will64@hotmail.com",
                FirstName = "Columbus",
                LastName = "Will",
                RegisteredAt = new DateTime(2020, 6, 17, 3, 38, 36, 748, DateTimeKind.Local).AddTicks(1173),
                Login = "User24Login",
                Password = "User24Password",
                Role = UserRoles.RegisteredUser,
                Balance = 93.00M
            },

            new User()
            {
                Id = 25,
                Email = "Alfred_Ward44@yahoo.com",
                FirstName = "Alfred",
                LastName = "Ward",
                RegisteredAt = new DateTime(2020, 6, 16, 15, 2, 16, 561, DateTimeKind.Local).AddTicks(1785),
                Login = "User25Login",
                Password = "User25Password",
                Role = UserRoles.RegisteredUser,
                Balance = 10.0M
            },

            new User()
            {
                Id = 26,
                Email = "Ada29@hotmail.com",
                FirstName = "Ada",
                LastName = "West",
                RegisteredAt = new DateTime(2020, 6, 12, 22, 59, 15, 838, DateTimeKind.Local).AddTicks(1128),
                Login = "User26Login",
                Password = "User26Password",
                Role = UserRoles.RegisteredUser,
                Balance = 23.00M
            },

            new User()
            {
                Id = 27,
                Email = "Dianna_Lynch@hotmail.com",
                FirstName = "Dianna",
                LastName = "Lynch",
                RegisteredAt = new DateTime(2020, 6, 22, 6, 11, 14, 555, DateTimeKind.Local).AddTicks(3721),
                Login = "User27Login",
                Password = "User27Password",
                Role = UserRoles.RegisteredUser,
                Balance = 923.00M
            },

            new User()
            {
                Id = 28,
                Email = "Hulda.Murray61@yahoo.com",
                FirstName = "Hulda",
                LastName = "Murray",
                RegisteredAt = new DateTime(2020, 6, 26, 14, 35, 49, 260, DateTimeKind.Local).AddTicks(625),
                Login = "User28Login",
                Password = "User28Password",
                Role = UserRoles.RegisteredUser,
                Balance = 120.0M
            },

            new User()
            {
                Id = 29,
                Email = "Beatrice.Kerluke@gmail.com",
                FirstName = "Beatrice",
                LastName = "Kerluke",
                RegisteredAt = new DateTime(2020, 6, 24, 1, 2, 32, 10, DateTimeKind.Local).AddTicks(2632),
                Login = "User29Login",
                Password = "User29Password",
                Role = UserRoles.RegisteredUser,
                Balance = 113.00M
            },

            new User()
            {
                Id = 30,
                Email = "Edgar_Gislason@hotmail.com",
                FirstName = "Edgar",
                LastName = "Gislason",
                RegisteredAt = new DateTime(2020, 6, 17, 2, 8, 50, 822, DateTimeKind.Local).AddTicks(7785),
                Login = "User30Login",
                Password = "User30Password",
                Role = UserRoles.RegisteredUser,
                Balance = 23.00M
            },

        };
        /// <summary>
        /// Seeding data containing products information.
        /// </summary>
        public static List<Product> Products { get; set; } = new List<Product>()
        {
            new Product()
            {
                Id = 0,
                Name = "Product1Name",
                Description = "Product1Description",
                Price = 90.25M,
                Category = ProductCategories.Category1
            },

            new Product()
            {
                Id = 1,
                Name = "Product2Name",
                Description = "Product2Description",
                Price = 35.59M,
                Category = ProductCategories.Category2
            },

            new Product()
            {
                Id = 2,
                Name = "Product3Name",
                Description = "Product3Description",
                Price = 115.00M,
                Category = ProductCategories.Category3
            },

            new Product()
            {
                Id = 3,
                Name = "Product4Name",
                Description = "Product4Description",
                Price = 90.25M,
                Category = ProductCategories.Category4
            },

            new Product()
            {
                Id = 4,
                Name = "Product5Name",
                Description = "Product5Description",
                Price = 20.25M,
                Category = ProductCategories.Category1
            },

            new Product()
            {
                Id = 5,
                Name = "Product6Name",
                Description = "Product6Description",
                Price = 340.25M,
                Category = ProductCategories.Category4
            },

            new Product()
            {
                Id = 6,
                Name = "Product7Name",
                Description = "Product7Description",
                Price = 10.25M,
                Category = ProductCategories.Category3
            },

            new Product()
            {
                Id = 7,
                Name = "Product8Name",
                Description = "Product8Description",
                Price = 90.25M,
                Category = ProductCategories.Category2
            },

            new Product()
            {
                Id = 8,
                Name = "Product9Name",
                Description = "Product9Description",
                Price = 20.25M,
                Category = ProductCategories.Category2
            },

            new Product()
            {
                Id = 9,
                Name = "Product10Name",
                Description = "Product10Description",
                Price = 340.25M,
                Category = ProductCategories.Category3
            },

            new Product()
            {
                Id = 10,
                Name = "Product11Name",
                Description = "Product11Description",
                Price = 10.25M,
                Category = ProductCategories.Category3
            },
        };
        /// <summary>
        /// Seeding data containing orders information.
        /// </summary>
        public static List<Order> Orders { get; set; } = new List<Order>()
        {
            new Order()
            {
                Id = 0,
                Items = new List<Product>() { Products[0], Products[2]},
                Total = Products[0].Price + Products[2].Price,
                UserId = 2,
                Status = OrderStatus.PaymentReceived,
                CreatedAt = new DateTime(2020, 7, 25, 19, 12, 11)
            },

            new Order()
            {
                Id = 1,
                Items = new List<Product>() { Products[1], Products[0]},
                Total = Products[1].Price + Products[0].Price,
                UserId = 0,
                Status = OrderStatus.Received,
                CreatedAt = new DateTime(2020, 7, 26, 22, 22, 39)
            },

            new Order()
            {
                Id = 2,
                Items = new List<Product>() { Products[2]},
                Total = Products[2].Price,
                UserId = 3,
                Status = OrderStatus.Completed,
                CreatedAt = new DateTime(2020, 7, 27, 16, 50, 14),
                FinishedAt = new DateTime(2020, 7, 28, 23, 50, 1)
            },

            new Order()
            {
                Id = 3,
                Items = new List<Product>() { Products[3]},
                Total = Products[3].Price,
                UserId = 23,
                Status = OrderStatus.New,
                CreatedAt = new DateTime(2020, 7, 27, 16, 50, 14),
                FinishedAt = new DateTime(2020, 7, 28, 23, 50, 1)
            }
        };
    }
}
