﻿using ConsoleEShop.DataAccess.Entities;
using System.Collections.Generic;

namespace ConsoleEShop.Repository.Context
{
    /// <summary>
    /// Context that stores collections of users, orders and products.
    /// </summary>
    public class ApplicationContext
    {
        /// <summary>
        /// List of all user accounts present in the system.
        /// </summary>
        public List<User> Users { get; set; } = DataSource.Users;
        /// <summary>
        /// List of all orders made by users.
        /// </summary>
        public List<Order> Orders { get; set; } = DataSource.Orders;
        /// <summary>
        /// List of all products in the shop.
        /// </summary>
        public List<Product> Products { get; set; } = DataSource.Products;
    }
}
