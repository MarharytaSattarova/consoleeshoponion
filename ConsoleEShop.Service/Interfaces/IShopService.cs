﻿using ConsoleEShop.DataAccess.Entities;
using ConsoleEShop.DataAccess.Enums;
using System.Collections.Generic;

namespace ConsoleEShop.Service.Interfaces
{
    /// <summary>
    /// Contains the functionality that Shop Service must have.
    /// </summary>
    public interface IShopService
    {
        /// <summary>
        /// Searches for the user instance by provided login and password.
        /// </summary>
        /// <param name="login">User login.</param>
        /// <param name="password">User password.</param>
        /// <returns>User instance if login was successful.</returns>
        public User LogIn(string login, string password);
        /// <summary>
        /// Adds user instance to the collection of user profiles in the shop if the user information provided is valid.
        /// </summary>
        /// <param name="newUserInfo">User instance with user's personal information.</param>
        /// <returns>User instance if registration was successful.</returns>
        public User Register(User newUserInfo);
        /// <summary>
        /// Gets all the products present in the shop.
        /// </summary>
        /// <returns>List of products.</returns>
        public List<Product> GetProducts();
        /// <summary>
        /// Gets the product with specified name.
        /// </summary>
        /// <param name="name">Name of the product to find and return.</param>
        /// <returns>Product instance or null if the product with given name does not exist in the collection.</returns>
        public Product GetProduct(string name);
        /// <summary>
        /// Adds provided order instance to the collection of orders in the store.
        /// </summary>
        /// <param name="newOrderInfo">Order instance to add to the collection.</param>
        /// <returns>Instance of the order added if the addition was successful.</returns>
        public Order CreateOrder(Order newOrderInfo);
        /// <summary>
        /// Processes the payment for the order if there is enough money on user's balance.
        /// </summary>
        /// <param name="currentUser">Current user instance.</param>
        /// <param name="orderId">Id of the order that has to be proceeded.</param>
        public void ProceedOrder(User currentUser, int orderId);
        /// <summary>
        /// Searches for the order by Id and sets it's status to CancelByUser.
        /// </summary>
        /// <param name="currentUser">User instance who canceles the order.</param>
        /// <param name="orderId">Idof the order to cancel.</param>
        public void CancelOrder(User currentUser, int orderId);
        /// <summary>
        /// Searches for the user instance by Id and returns this user's orders.
        /// </summary>
        /// <param name="userId">Id of the user whose orders are needed to get.</param>
        /// <returns>List of orders of the given user.</returns>
        public List<Order> GetUserOrders(int userId);
        /// <summary>
        /// Searches for the order by provided Id and sets it's status.
        /// </summary>
        /// <param name="orderId">Id of the order to set status.</param>
        /// <param name="newStatus">New status that has to be set for the order.</param>
        public void SetOrderStatus(int orderId, OrderStatus newStatus);
        /// <summary>
        /// Searches for an user by Id and updates it's information with the information provided in the user object argument.
        /// </summary>
        /// <param name="userId">Id of the user that has to be updated.</param>
        /// <param name="newUserInfo">User instance containig new user information.</param>
        public void UpdateUserInfo(int userId, User newUserInfo);
        /// <summary>
        /// Searches for user instance by Id and returns it.
        /// </summary>
        /// <param name="userId">Id of the user to get info about.</param>
        /// <returns>User instance.</returns>
        public User GetUserInfo(int userId);
        /// <summary>
        /// Gets all the users present in the shop.
        /// </summary>
        /// <returns>List of users.</returns>
        public List<User> GetUsers();
        /// <summary>
        /// Adds provided product instance to the collection of products in the shop.
        /// </summary>
        /// <param name="product">Product instance to add to the collection.</param>
        /// <returns>Product instance if the addition was successful.</returns>
        public Product CreateProduct(Product product);
        /// <summary>
        /// Searches for the product instance by Id and updates information about it.
        /// </summary>
        /// <param name="productId">Id of the product to update.</param>
        /// <param name="newProductInfo">Product instance containing new product information.</param>
        public void UpdateProductInfo(int productId, Product newProductInfo);
    }
}
