﻿using ConsoleEShop.Service.Interfaces;
using System;
using System.Collections.Generic;
using ConsoleEShop.Service.ShopModel;
using ConsoleEShop.DataAccess.Entities;
using System.Linq;
using System.Text.RegularExpressions;
using ConsoleEShop.DataAccess.Enums;

namespace ConsoleEShop.Service.Implementation
{
    /// <summary>
    /// Implements all the functionality that is available in the shop application.
    /// </summary>
    public class ShopService : IShopService
    {
        /// <summary>
        /// Stores the singleton shop instance.
        /// </summary>
        private readonly Shop shopInstance;
        /// <summary>
        /// Costructor that creates an instance of the Shop Service.
        /// </summary>
        public ShopService()
        {
            shopInstance = Shop.Instance;
        }
        /// <summary>
        /// Searches for the user instance by provided login and password.
        /// </summary>
        /// <param name="login">User login.</param>
        /// <param name="password">User password.</param>
        /// <returns>User instance if login was successful.</returns>
        /// <exception cref="InvalidOperationException">Thrown when user profile with provided login and password is not found in the shop."</exception>
        public User LogIn(string login, string password)
        {
            var currentUser = shopInstance.UnitOfWork.Users.Get().FirstOrDefault(x => x.Login == login && x.Password == password);

            if (currentUser == null) throw new InvalidOperationException("User with such login and password not found.");

            return currentUser;
        }
        /// <summary>
        /// Adds user instance to the collection of user profiles in the shop if the user information provided is valid.
        /// </summary>
        /// <param name="newUserInfo">User instance with user's personal information.</param>
        /// <returns>User instance if registration was successful.</returns>
        /// <exception cref="ArgumentException">Thrown when any of user's information was invalid."</exception>
        public User Register(User newUserInfo)
        {
            if (string.IsNullOrWhiteSpace(newUserInfo.Login) || string.IsNullOrWhiteSpace(newUserInfo.Password)) throw new ArgumentException("Login/Password cannot be an empty string.");
            if (!IsValidName(newUserInfo.FirstName, newUserInfo.LastName)) throw new ArgumentException("First name/last name field has invalid value.");
            if (!IsValidEmail(newUserInfo.Email)) throw new ArgumentException("Invalid email.");

            newUserInfo.Id = shopInstance.UnitOfWork.Users.Get().Count;
            shopInstance.UnitOfWork.Users.Create(newUserInfo);

            return newUserInfo;
        }
        /// <summary>
        /// Checks if the user email is valid.
        /// </summary>
        /// <param name="email">User email.</param>
        /// <returns>True if email is valid and false if it's not.</returns>
        private bool IsValidEmail(string email)
        {
            var pattern = new Regex(@"^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$", RegexOptions.IgnoreCase);

            return pattern.IsMatch(email);
        }
        /// <summary>
        /// Checks if the user first name and last name are valid.
        /// </summary>
        /// <param name="firstName">User first name.</param>
        /// <param name="lastName">User last name.</param>
        /// <returns>True if both first name and last name are valid and false if they are not.</returns>
        private bool IsValidName(string firstName, string lastName)
        {
            var pattern = new Regex(@"^([^\s]+)$", RegexOptions.IgnoreCase);

            if (pattern.IsMatch(firstName) && pattern.IsMatch(lastName))
                return true;

            return false;

        }
        /// <summary>
        /// Gets all the products present in the shop.
        /// </summary>
        /// <returns>List of products.</returns>
        public List<Product> GetProducts()
        {
            return shopInstance.UnitOfWork.Products.Get();
        }
        /// <summary>
        /// Gets the product with specified name.
        /// </summary>
        /// <param name="name">Name of the product to find and return.</param>
        /// <returns>Product instance or null if the product with given name does not exist in the collection.</returns>
        /// <exception cref="ArgumentException">Thrown when product with the provided name is not found in the shop."</exception>
        public Product GetProduct(string name)
        {
            var searchedProduct = shopInstance.UnitOfWork.Products.Get().FirstOrDefault(x => x.Name == name);

            if (searchedProduct == null) throw new ArgumentException("Product with such name not found.");

            return searchedProduct;
        }
        /// <summary>
        /// Gets the product with specified Id.
        /// </summary>
        /// <param name="id">Id of the product to find and return.</param>
        /// <returns>Product instance or null if the product with given Id does not exist in the collection.</returns>
        /// <exception cref="ArgumentException">Thrown when product with the provided Id is not found in the shop."</exception>
        public Product GetProduct(int id)
        {
            var searchedProduct = shopInstance.UnitOfWork.Products.Get(id);

            if (searchedProduct == null) throw new ArgumentException("Product with such Id not found.");

            return searchedProduct;
        }
        /// <summary>
        /// Adds provided order instance to the collection of orders in the store.
        /// </summary>
        /// <param name="newOrderInfo">Order instance to add to the collection.</param>
        /// <returns>Instance of the order added if the addition was successful.</returns>
        /// <exception cref="InvalidOperationException">Thrown when user with the Id provided for order's field UserId does not exist."</exception>
        public Order CreateOrder(Order newOrderInfo)
        {
            if (shopInstance.UnitOfWork.Users.Get().FirstOrDefault(x => x.Id == newOrderInfo.UserId) == null) throw new InvalidOperationException("User with Id specified in the field UserId does not exist.");
            if (!newOrderInfo.Items.Any()) throw new InvalidOperationException("Unable to create order. Items list is empty");

            newOrderInfo.Id = shopInstance.UnitOfWork.Orders.Get().Count;
            newOrderInfo.Status = OrderStatus.New;
            newOrderInfo.CreatedAt = DateTime.Now;

            shopInstance.UnitOfWork.Orders.Create(newOrderInfo);

            return newOrderInfo;
        }
        /// <summary>
        /// Processes the payment for the order if there is enough money on user's balance.
        /// </summary>
        /// <param name="currentUser">Current user instance.</param>
        /// <param name="orderId">Id of the order that has to be proceeded.</param>
        /// <exception cref="ArgumentException">Thrown when order with the provided Id is not found."</exception>
        /// <exception cref="InvalidOperationException">Thrown when order is already payed or order status is Sent or CanceledByAdmin."</exception>
        public void ProceedOrder(User currentUser, int orderId)
        {
            var order = shopInstance.UnitOfWork.Orders.Get().FirstOrDefault(x => x.Id == orderId && x.UserId == currentUser.Id);

            if (order == null) throw new ArgumentException("Order with such id not found.", "orderId");

            if (order.Status == OrderStatus.PaymentReceived || order.Status == OrderStatus.Sent) throw new InvalidOperationException("This order is already in progress.");

            if (order.Status == OrderStatus.CanceledByAdmin) throw new InvalidOperationException("This order has been canceled by administrator.");

            Pay(currentUser, order);
        }
        /// <summary>
        /// Subtracts the order total from the user's balance.
        /// </summary>
        /// <param name="user">The instance of the userwho pays for the order.</param>
        /// <param name="order">The instance of the order that is going to be payed.</param>
        /// <exception cref="InvalidOperationException">Thrown when there is not enough money on user's balance to pay for the order."</exception>
        private void Pay(User user, Order order)
        {
            if (user.Balance < order.Total) throw new InvalidOperationException("Not enough money on the balance to pay for this order.");

            user.Balance -= order.Total;
        }
        /// <summary>
        /// Searches for the order by Id and sets it's status to CancelByUser.
        /// </summary>
        /// <param name="currentUser">User instance who canceles the order.</param>
        /// <param name="orderId">Idof the order to cancel.</param>
        /// <exception cref="InvalidOperationException">Thrown when order with provided Id is not found or when order has been already received or canceled."</exception>
        public void CancelOrder(User currentUser, int orderId)
        {
            var order = shopInstance.UnitOfWork.Orders.Get().FirstOrDefault(x => x.Id == orderId && x.UserId == currentUser.Id);

            if (order == null) throw new InvalidOperationException("Order with such id not found.");

            if (order.Status == OrderStatus.Received) throw new InvalidOperationException("Unable to cancel order because order status is 'Received'.");

            if (order.Status == OrderStatus.CanceledByAdmin || order.Status == OrderStatus.CanceledByUser) throw new InvalidOperationException("Order is already canceled.");

            if (order.Status != OrderStatus.New)
            {
                currentUser.Balance += order.Total;
            }

            order.Status = OrderStatus.CanceledByUser;
        }
        /// <summary>
        /// Searches for the user instance by Id and returns this user's orders.
        /// </summary>
        /// <param name="userId">Id of the user whose orders are needed to get.</param>
        /// <returns>List of orders of the given user.</returns>
        /// <exception cref="ArgumentException">Thrown when user with the provided Id is not found."</exception>
        public List<Order> GetUserOrders(int userId)
        {
            if (shopInstance.UnitOfWork.Users.Get().FirstOrDefault(x => x.Id == userId) == null) throw new ArgumentException("User with such Id does not exist.", "userId");

            return shopInstance.UnitOfWork.Orders.Get().Where(x => x.UserId == userId).ToList();
        }
        /// <summary>
        /// Searches for the order by provided Id and sets it's status.
        /// </summary>
        /// <param name="orderId">Id of the order to set status.</param>
        /// <param name="newStatus">New status that has to be set for the order.</param>
        /// <exception cref="ArgumentException">Thrown when order with the provided Id is not found."</exception>
        public void SetOrderStatus(int orderId, OrderStatus newStatus)
        {
            var searchedOrder = shopInstance.UnitOfWork.Orders.Get().FirstOrDefault(x => x.Id == orderId);

            if (searchedOrder == null) throw new ArgumentException("Order with such Id not found.", "orderId");

            searchedOrder.Status = newStatus;

            if (newStatus == OrderStatus.Completed)
                searchedOrder.FinishedAt = DateTime.Now;
        }
        /// <summary>
        /// Searches for an user by Id and updates it's information with the information provided in the user object argument.
        /// </summary>
        /// <param name="userId">Id of the user that has to be updated.</param>
        /// <param name="newUserInfo">User instance containig new user information.</param>
        /// <exception cref="InvalidOperationException">Thrown when name, login, password or email provided in the user instance is invalid."</exception>
        public void UpdateUserInfo(int userId, User newUserInfo)
        {
            if (!IsValidName(newUserInfo.FirstName, newUserInfo.LastName)) throw new InvalidOperationException("Name is invalid.");
            if (!IsValidEmail(newUserInfo.Email)) throw new InvalidOperationException("Email is invalid.");
            if (string.IsNullOrWhiteSpace(newUserInfo.Login) || string.IsNullOrWhiteSpace(newUserInfo.Password)) throw new InvalidOperationException("Login/password cannot be an empty string.");

            shopInstance.UnitOfWork.Users.Update(newUserInfo, userId);
        }
        /// <summary>
        /// Puts the given amount of money on user's balance.
        /// </summary>
        /// <param name="sum">Amount of money to put on the balance.</param>
        /// <param name="user">User instance whose balance has to be topped up.</param>
        /// <exception cref="InvalidOperationException">Thrown when sum is equal to or less than zero."</exception>
        public void TopUpUserBalance(decimal sum, User user)
        {
            if (sum <= 0) throw new InvalidOperationException("Sum to put on the balance has to be a non-zero value.");

            user.Balance += sum;
        }
        /// <summary>
        /// Searches for user instance by Id and returns it.
        /// </summary>
        /// <param name="userId">Id of the user to get info about.</param>
        /// <returns>User instance.</returns>
        public User GetUserInfo(int userId)
        {
            return shopInstance.UnitOfWork.Users.Get(userId);
        }
        /// <summary>
        /// Gets all the users present in the shop.
        /// </summary>
        /// <returns>List of users.</returns>
        public List<User> GetUsers()
        {
            return shopInstance.UnitOfWork.Users.Get();
        }
        /// <summary>
        /// Adds provided product instance to the collection of products in the shop.
        /// </summary>
        /// <param name="product">Product instance to add to the collection.</param>
        /// <returns>Product instance if the addition was successful.</returns>
        /// <exception cref="InvalidOperationException">Thrown when product name is invalid or product price is equal to or less than zero."</exception>
        public Product CreateProduct(Product product)
        {
            if (string.IsNullOrWhiteSpace(product.Name)) throw new InvalidOperationException("Product name cannot be an empty string.");
            if (product.Price <= 0) throw new InvalidOperationException("Product price must be a non-zero value.");

            product.Id = shopInstance.UnitOfWork.Products.Get().Count;

            shopInstance.UnitOfWork.Products.Create(product);

            return product;
        }
        /// <summary>
        /// Searches for the product instance by Id and updates information about it.
        /// </summary>
        /// <param name="productId">Id of the product to update.</param>
        /// <param name="newProductInfo">Product instance containing new product information.</param>
        /// <exception cref="InvalidOperationException">Thrown when product name or description is invalid."</exception>
        public void UpdateProductInfo(int productId, Product newProductInfo)
        {
            if (string.IsNullOrWhiteSpace(newProductInfo.Name) || string.IsNullOrWhiteSpace(newProductInfo.Description))
                throw new InvalidOperationException("Product name/description cannot be an empty string.");

            if (newProductInfo.Price <= 0) throw new InvalidOperationException("Product price must be a non-zero number.");

            shopInstance.UnitOfWork.Products.Update(newProductInfo, productId);
        }
    }
}
