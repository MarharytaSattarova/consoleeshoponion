﻿using ConsoleEShop.Repository.Interfaces;
using ConsoleEShop.Repository.UnitOfWork;

namespace ConsoleEShop.Service.ShopModel
{
    /// <summary>
    /// Represent Singleton shop entity.
    /// </summary>
    public class Shop
    {
        /// <summary>
        /// Stores the shop instance.
        /// </summary>
        private static Shop instance;
        /// <summary>
        /// Stores the object locker.
        /// </summary>
        private static readonly object locker = new object();
        /// <summary>
        /// Creates and stores an UnitOfWork instance.
        /// </summary>
        public IUnitOfWork UnitOfWork { get; set; } = new UnitOfWork();
        /// <summary>
        /// Private parameterless constructor.
        /// </summary>
        Shop() { }
        /// <summary>
        /// Stores the shop instance and provides access only for reading, if the shop instance does not exist than creates it.
        /// </summary>
        public static Shop Instance
        {
            get
            {
                lock (locker)
                {
                    return instance ?? (instance = new Shop());
                }
            }
        }
    }
}
