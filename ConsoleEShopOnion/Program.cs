﻿using ConsoleEShop.Service.Implementation;
using ConsoleEShop.UI.MenuProvider;

namespace ConsoleEShopOnion
{
    /// <summary>
    /// Starting point for the application.
    /// </summary>
    public static class Program
    {
        /// <summary>
        /// Main method.
        /// </summary>
        static void Main()
        {
            var shopService = new ShopService();

            while (MenuProvider.toContinue)
                MenuProvider.ProvideMenu(shopService);
        }
    }
}
