﻿using ConsoleEShop.DataAccess.Entities;
using ConsoleEShop.DataAccess.Enums;
using ConsoleEShop.Service.Implementation;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ConsoleEShop.UI.MenuProvider
{
    /// <summary>
    /// Provides user interface options and handles user's interactions with the system.
    /// </summary>
    public static class MenuProvider
    {
        /// <summary>
        /// Stores true if the user interface menu has to circularly appear on the console and false when user chooses to exit.
        /// </summary>
        internal static bool toContinue = true;
        /// <summary>
        /// Stores the user's input equivalent to the number of particular action.
        /// </summary>
        private static int actionChoiceInput;
        /// <summary>
        /// Instance of the Shop Service.
        /// </summary>
        private static ShopService shopService;
        /// <summary>
        /// User interface options for the user with role Guest.
        /// </summary>
        private static string menuOptionsForGuest = "Choose action:\n" +
                                                    "0 - Exit \n"
                                                  + "1 - Log In \n"
                                                  + "2 - Register \n"
                                                  + "3 - View products \n"
                                                  + "4 - Get product by name \n";
        /// <summary>
        /// User interface options for the user with role Registered User.
        /// </summary>
        private static string menuOptionsforUser = "Choose action:\n" +
                                                   " 0 - Exit \n"
                                                 + " 1 - View products \n"
                                                 + " 2 - Get product by name \n"
                                                 + " 3 - Create order \n"
                                                 + " 4 - Proceed order \n"
                                                 + " 5 - Cancel order \n"
                                                 + " 6 - View orders history \n"
                                                 + " 7 - Set 'Received' order status \n"
                                                 + " 8 - View profile information \n"
                                                 + " 9 - Update profile information \n"
                                                 + "10 - Top up balance \n"
                                                 + "11 - Log Out \n";
        /// <summary>
        /// User interface options for the user with role Admin.
        /// </summary>
        private static string menuOptionsforAdmin = "Choose action:\n" +
                                                    " 0 - Exit \n"
                                                  + " 1 - View products \n"
                                                  + " 2 - Get product by name \n"
                                                  + " 3 - Create order \n"
                                                  + " 4 - Proceed order \n"
                                                  + " 5 - View all users' info \n"
                                                  + " 6 - View user's info by Id \n"
                                                  + " 7 - Update user's info by Id \n"
                                                  + " 8 - Add product \n"
                                                  + " 9 - Update product info \n"
                                                  + "10 - Top up balance \n"
                                                  + "11 - Set order status \n"
                                                  + "12 - Log Out \n";
        /// <summary>
        /// Dictionary which stores different user interface option depending on the user role.
        /// </summary>
        private static Dictionary<UserRoles, string> UserRolesWithOptions = new Dictionary<UserRoles, string>()
        {
            { UserRoles.Guest, menuOptionsForGuest},
            { UserRoles.RegisteredUser, menuOptionsforUser},
            { UserRoles.Admin, menuOptionsforAdmin }
        };
        /// <summary>
        /// Array of methods to handle interface options available for Guest.
        /// </summary>
        private static Action[] menuActionsForGuest = new Action[] { Exit, LogIn, Register, GetProducts, GetProductByName };
        /// <summary>
        /// Array of methods to handle interface options available for Registered User.
        /// </summary>
        private static Action[] menuActionsForUser = new Action[] { Exit, GetProducts, GetProductByName, CreateOrder, ProceedOrder, CancelOrder, GetOrderHistory,
                                                    SetReceivedStatus, ViewUserInfo, UpdateUserInfo, TopUpUserBalance, LogOut };
        /// <summary>
        /// Array of methods to handle interface options available for Admin.
        /// </summary>
        private static Action[] menuActionsForAdmin = new Action[] { Exit, GetProducts, GetProductByName, CreateOrder, ProceedOrder, ViewAllUsersInfo, ViewUserInfoById,
                                                    UpdateUserInfoById, CreateProduct, UpdateProductInfo, TopUpUserBalance, SetOrderStatus, LogOut };
        /// <summary>
        /// Dictionary which stores different user interface action methods depending on the user role.
        /// </summary>
        private static Dictionary<UserRoles, Action[]> UserRolesWithActions = new Dictionary<UserRoles, Action[]>()
        {
            { UserRoles.Guest, menuActionsForGuest},
            { UserRoles.RegisteredUser, menuActionsForUser},
            { UserRoles.Admin, menuActionsForAdmin},
        };
        /// <summary>
        /// Stores the instance of the current user.
        /// </summary>
        private static User currentUser;
        /// <summary>
        /// Stores the role of the current user. Guest by default.
        /// </summary>
        private static UserRoles currentUserRole = UserRoles.Guest;
        /// <summary>
        /// Static constructor of the MenuProvider class.
        /// </summary>
        static MenuProvider() { }
        /// <summary>
        /// Shows available user interface options on the console. 
        /// </summary>
        /// <param name="incomingShopService">Shop Service instance to be set as a value to the corresponding MenuProvider field.</param>
        public static void ProvideMenu(ShopService incomingShopService)
        {
            MenuProvider.shopService = incomingShopService;
            var menuForCurrentUser = UserRolesWithOptions[currentUserRole];
            var optionsForCurrentUser = UserRolesWithActions[currentUserRole];
            Console.WriteLine(menuForCurrentUser);

            try
            {
                Console.Write("Your input: ");
                var actionChoiceInputString = Console.ReadLine();
                actionChoiceInput = Convert.ToInt32(actionChoiceInputString);
                optionsForCurrentUser[actionChoiceInput]();
            }
            catch (FormatException)
            {
                Console.WriteLine("User input is not a number");
            }
            catch (IndexOutOfRangeException)
            {
                Console.WriteLine($"Input error. Enter number from 0 to {optionsForCurrentUser.Length}");
            }
            catch (Exception exception)
            {
                Console.WriteLine($"Exception: {exception.Message}");
            }

            Console.WriteLine();
        }
        /// <summary>
        /// Shuts the application down by setting toContinue to false.
        /// </summary>
        private static void Exit()
        {
            toContinue = false;
        }
        /// <summary>
        /// Gets login and password from the user input and calls Service LogIn method to process login.
        /// </summary>
        private static void LogIn()
        {
            try
            {
                Console.Write("\nLogin: ");
                var loginInput = Console.ReadLine();

                Console.Write("\nPassword: ");
                var passwordInput = Console.ReadLine();

                var user = shopService.LogIn(loginInput, passwordInput);

                Console.WriteLine($"\n Hello, {user.FirstName} {user.LastName}!");

                currentUser = user;
                currentUserRole = user.Role;

            }
            catch (Exception ex)
            {
                Console.WriteLine("Error" + ex.Message);
            }
        }
        /// <summary>
        /// Gets user information from the user input and calls Service Register method to complete registration process.
        /// </summary>
        private static void Register()
        {
            try
            {
                Console.Write("\nEnter Login: ");
                var loginInput = Console.ReadLine();

                Console.Write("\nEnter Password: ");
                var passwordInput = Console.ReadLine();

                Console.Write("\nEnter First Name: ");
                var firstNameInput = Console.ReadLine();

                Console.Write("\nEnter Last Name: ");
                var lastNameInput = Console.ReadLine();

                Console.Write("\nEnter Email: ");
                var emailInput = Console.ReadLine();

                var newUser = new User()
                {
                    Login = loginInput,
                    Password = passwordInput,
                    FirstName = firstNameInput,
                    LastName = lastNameInput,
                    Email = emailInput,
                    Role = UserRoles.RegisteredUser,
                    RegisteredAt = DateTime.Now
                };

                var user = shopService.Register(newUser);

                Console.WriteLine($"\n Welcome, {user.FirstName} {user.LastName}! \nYour profile successfuly created.");

                currentUser = user;
                currentUserRole = user.Role;

            }
            catch (Exception ex)
            {
                Console.WriteLine("Error" + ex.Message);
            }
        }
        /// <summary>
        /// Gets all the products present in the shop.
        /// </summary>
        private static void GetProducts()
        {
            try
            {
                Console.WriteLine("\nProducts: \n");

                var productsList = shopService.GetProducts();

                foreach (var item in productsList)
                {
                    Console.WriteLine($"Name: {item.Name} \nDescription: {item.Description} \nCategory: {item.Category} \nPrice: {item.Price}");
                    Console.WriteLine();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error" + ex.Message);
            }
        }
        /// <summary>
        /// Gets product name from the user input and calls for the Service GetProduct method to find the information about the product with the provided name.
        /// </summary>
        private static void GetProductByName()
        {
            try
            {
                Console.Write("\nEnter product name: ");
                var input = Console.ReadLine();

                var result = shopService.GetProduct(input);

                Console.WriteLine("\nProduct found:");
                Console.WriteLine($"Name: {result.Name} \nDescription: {result.Description} \nCategory: {result.Category} \nPrice: {result.Price}");

            }
            catch (Exception ex)
            {
                Console.WriteLine("Error" + ex.Message);
            }
        }
        /// <summary>
        /// Gets order information from the user input and calls the Service CreateOrder method to add the order to the collection of orders in the shop.
        /// </summary>
        private static void CreateOrder()
        {
            try
            {
                var input = string.Empty;
                var productsList = new List<Product>();

                while (true)
                {
                    Console.WriteLine("Enter item id to add to your order or X to continue: ");
                    input = Console.ReadLine();

                    if (input.ToUpper() == "X")
                        break;

                    var productId = Convert.ToInt32(input);
                    var searchedProduct = shopService.GetProducts().FirstOrDefault(x => x.Id == productId);

                    if (searchedProduct == null)
                    {
                        Console.WriteLine("Product with such Id not found.");
                        continue;
                    }

                    productsList.Add(searchedProduct);
                    Console.WriteLine($"{searchedProduct.Name} (Id: {searchedProduct.Id}) added to your order.");
                }

                decimal total = 0;

                foreach (var item in productsList)
                {
                    total += item.Price;
                }

                var order = new Order() { Items = productsList, Total = total, CreatedAt = DateTime.Now, Status = OrderStatus.New, UserId = currentUser.Id };
                var createdOrder = shopService.CreateOrder(order);

                Console.WriteLine("Order successfuly created. Info:");
                Console.WriteLine("Items:");

                foreach (var item in createdOrder.Items)
                {
                    Console.WriteLine($"Id: {item.Id} Name: {item.Name} Price: {item.Price}");
                }

                Console.WriteLine($"\n Total : {createdOrder.Total}");
                Console.WriteLine($"To proceed this order use Id: {createdOrder.Id}");

            }
            catch (Exception ex)
            {
                Console.WriteLine("Error" + ex.Message);
            }
        }
        /// <summary>
        /// Gets order Id from the user input and calls the Service ProceedOrder method.
        /// </summary>
        private static void ProceedOrder()
        {
            try
            {
                Console.Write("Enter order Id:");
                var orderId = Convert.ToInt32(Console.ReadLine());

                shopService.ProceedOrder(currentUser, orderId);

                Console.WriteLine($"Order successfuly proceeded. To check order status use Id : {orderId}");
                Console.WriteLine($"Your current balance: {currentUser.Balance}");
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error " + ex.Message);
            }
        }
        /// <summary>
        /// Gets order Id from the user input and calls the Service CancelOrder method to process order cancelation.
        /// </summary>
        private static void CancelOrder()
        {
            try
            {
                Console.Write("Enter order Id:");
                var orderId = Convert.ToInt32(Console.ReadLine());

                shopService.CancelOrder(currentUser, orderId);

                Console.WriteLine($"Order #{orderId} successfuly canceled.");
                Console.WriteLine($"Your current balance: {currentUser.Balance}");
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error " + ex.Message);
            }
        }
        /// <summary>
        /// Shows all the orders made by the current user.
        /// </summary>
        private static void GetOrderHistory()
        {
            try
            {
                Console.Write("Orders: \n");

                var userOrders = shopService.GetUserOrders(currentUser.Id);

                foreach (var element in userOrders)
                {
                    Console.WriteLine($"Order Id: {element.Id}");
                    Console.WriteLine($"Order Status: {element.Status}");
                    Console.WriteLine($"Items:");
                    foreach (var item in element.Items)
                    {
                        Console.WriteLine($"Product Id: {item.Id} Name: {item.Name}");
                    }

                    Console.WriteLine($"\n Total: {element.Total}");
                    Console.WriteLine();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error " + ex.Message);
            }
        }
        /// <summary>
        /// Gets order Id from the user input and sets the order status to Received.
        /// </summary>
        private static void SetReceivedStatus()
        {
            try
            {
                Console.Write("Enter order Id:");
                var orderId = Convert.ToInt32(Console.ReadLine());

                shopService.SetOrderStatus(orderId, OrderStatus.Received);

                Console.WriteLine($"Order #{orderId} status successfuly set to 'Received'.");
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error " + ex.Message);
            }
        }
        /// <summary>
        /// Shows current user profile information.
        /// </summary>
        private static void ViewUserInfo()
        {
            try
            {
                Console.WriteLine($"User Id: {currentUser.Id}");
                Console.WriteLine($"First Name: {currentUser.FirstName}");
                Console.WriteLine($"Last Name: {currentUser.LastName}");
                Console.WriteLine($"Email: {currentUser.Email}");
                Console.WriteLine($"Registered: {currentUser.RegisteredAt}");
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error " + ex.Message);
            }
        }
        /// <summary>
        /// Gets user Id from input and calls the Service GetUserInfo method to show the profile information of the user with the provided Id.
        /// </summary>
        private static void ViewUserInfoById()
        {
            try
            {
                Console.Write("Enter User Id:");
                var userId = Convert.ToInt32(Console.ReadLine());

                var user = shopService.GetUserInfo(userId);

                Console.WriteLine($"User Id: {user.Id}");
                Console.WriteLine($"First Name: {user.FirstName}");
                Console.WriteLine($"Last Name: {user.LastName}");
                Console.WriteLine($"Email: {user.Email}");
                Console.WriteLine($"Registered: {user.RegisteredAt}");
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error " + ex.Message);
            }
        }
        /// <summary>
        /// Shows profile information for all the user accounts present in the system.
        /// </summary>
        private static void ViewAllUsersInfo()
        {
            try
            {
                var users = shopService.GetUsers();

                foreach (var element in users)
                {
                    Console.WriteLine($"User Id: {element.Id}");
                    Console.WriteLine($"First Name: {element.FirstName}");
                    Console.WriteLine($"Last Name: {element.LastName}");
                    Console.WriteLine($"Email: {element.Email}");
                    Console.WriteLine($"Registered: {element.RegisteredAt}");
                    Console.WriteLine();
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine("Error " + ex.Message);
            }
        }
        /// <summary>
        /// Gets new user information from input and calls the Service UpdateUserInfo method to update profile information of the current user.
        /// </summary>
        private static void UpdateUserInfo()
        {
            try
            {
                Console.Write("\nEnter Login: ");
                var loginInput = Console.ReadLine();

                Console.Write("\nEnter Password: ");
                var passwordInput = Console.ReadLine();

                Console.Write("\nEnter First Name: ");
                var firstNameInput = Console.ReadLine();

                Console.Write("\nEnter Last Name: ");
                var lastNameInput = Console.ReadLine();

                Console.Write("\nEnter Email: ");
                var emailInput = Console.ReadLine();

                var newUserInfo = new User()
                {
                    Login = loginInput,
                    Password = passwordInput,
                    FirstName = firstNameInput,
                    LastName = lastNameInput,
                    Email = emailInput,
                };

                shopService.UpdateUserInfo(currentUser.Id, newUserInfo);

                Console.WriteLine($"Your profile successfuly updted.");
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error" + ex.Message);
            }
        }
        /// <summary>
        /// Gets user Id and new user information from input and calls the Service UpdateUserInfo method to update profile information of the user with provided Id.
        /// </summary>
        private static void UpdateUserInfoById()
        {
            try
            {
                Console.Write("Enter User Id:");
                var userId = Convert.ToInt32(Console.ReadLine());

                var user = shopService.GetUserInfo(userId);

                Console.Write("\nEnter Login: ");
                var loginInput = Console.ReadLine();

                Console.Write("\nEnter Password: ");
                var passwordInput = Console.ReadLine();

                Console.Write("\nEnter First Name: ");
                var firstNameInput = Console.ReadLine();

                Console.Write("\nEnter Last Name: ");
                var lastNameInput = Console.ReadLine();

                Console.Write("\nEnter Email: ");
                var emailInput = Console.ReadLine();

                var newUserInfo = new User()
                {
                    Login = loginInput,
                    Password = passwordInput,
                    FirstName = firstNameInput,
                    LastName = lastNameInput,
                    Email = emailInput,
                };

                shopService.UpdateUserInfo(userId, newUserInfo);

                Console.WriteLine($"User's profile (Id: {user.Id}) successfuly updated.");
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error" + ex.Message);
            }
        }
        /// <summary>
        /// Gets the amount of money from user input and calls the Service TopUpUserBalance method to put the provided sum on the user's balance.
        /// </summary>
        private static void TopUpUserBalance()
        {
            try
            {
                Console.Write("Enter the amount to put on your balance: ");
                var amount = Convert.ToDecimal(Console.ReadLine());

                shopService.TopUpUserBalance(amount, currentUser);

                Console.WriteLine($"Balance successfuly topped up. Your current balance: {currentUser.Balance}");
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error " + ex.Message);
            }
        }
        /// <summary>
        /// Gets product information from user input and calls the Service CreateProduct method to process the addition of new product to the shop.
        /// </summary>
        private static void CreateProduct()
        {
            try
            {
                Console.Write("\nEnter Product Name: ");
                var nameInput = Console.ReadLine();

                Console.Write("\nEnter Product Description: ");
                var descriptionInput = Console.ReadLine();

                Console.Write("\nEnter Product Category: ");
                var categoryInput = Convert.ToInt32(Console.ReadLine());

                Console.Write("\nEnter Product Price: ");
                var priceInput = Convert.ToDecimal(Console.ReadLine());

                var newProduct = new Product()
                {
                    Name = nameInput,
                    Description = descriptionInput,
                    Category = (ProductCategories)categoryInput,
                    Price = priceInput
                };

                var addedProduct = shopService.CreateProduct(newProduct);

                Console.WriteLine($"Product successfuly created. Id: {addedProduct.Id}");
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error" + ex.Message);
            }
        }
        /// <summary>
        /// Gets product Id and new product information from user input and calls the Service UpdateProductInfo method to update information about the product with provided Id.
        /// </summary>
        private static void UpdateProductInfo()
        {
            try
            {

                Console.Write("\nEnter Product Id: ");
                var idInput = Convert.ToInt32(Console.ReadLine());

                var searchedProduct = shopService.GetProduct(idInput);

                Console.Write("\nEnter Product Name: ");
                var nameInput = Console.ReadLine();

                Console.Write("\nEnter Product Description: ");
                var descriptionInput = Console.ReadLine();

                Console.Write("\nEnter Product Category: ");
                var categoryInput = Convert.ToInt32(Console.ReadLine());

                Console.Write("\nEnter Product Price: ");
                var priceInput = Convert.ToDecimal(Console.ReadLine());

                var newProductInfo = new Product()
                {
                    Name = nameInput,
                    Description = descriptionInput,
                    Category = (ProductCategories)categoryInput,
                    Price = priceInput
                };

                shopService.UpdateProductInfo(searchedProduct.Id, newProductInfo);

                Console.WriteLine($"Product #{searchedProduct.Id} successfuly updated.");
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error" + ex.Message);
            }
        }
        /// <summary>
        /// Gets order Id and new order status from user input and calls the Service SetOrderStatus method to update order status.
        /// </summary>
        private static void SetOrderStatus()
        {
            try
            {
                Console.Write("Enter order Id:");
                var orderId = Convert.ToInt32(Console.ReadLine());

                Console.Write("Enter order Status:");
                var orderStatus = Convert.ToInt32(Console.ReadLine());

                if ((OrderStatus)orderStatus == OrderStatus.CanceledByUser || (OrderStatus)orderStatus == OrderStatus.Received)
                {
                    Console.WriteLine("'Canceled by user' and 'Received' order status cannot be set by administrator.");
                    return;
                }

                shopService.SetOrderStatus(orderId, (OrderStatus)orderStatus);

                Console.WriteLine($"Order #{orderId} status successfuly set to '{(OrderStatus)orderStatus}'.");
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error " + ex.Message);
            }
        }
        /// <summary>
        /// Processes user logout by setting currentUser value to Guest.
        /// </summary>
        private static void LogOut()
        {
            try
            {
                currentUserRole = UserRoles.Guest;
                currentUser = null;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error " + ex.Message);
            }
        }
    }
}
